/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controlador;

import cl.model.Calculadora;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Personal
 */
@WebServlet(name = "CalculadoraControlador", urlPatterns = {"/CalculadoraControlador"})
public class CalculadoraControlador extends HttpServlet {

    private Calculadora cal;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   /**protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            /*out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CalculadoraControlador</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CalculadoraControlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
     */

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /*@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    request.getRequestDispatcher("resultado.jsp").forward(request, response);
    
    }
    */

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     
              
        String capital= request.getParameter("capital");
        String anios= request.getParameter("anios");
        String interes= request.getParameter("interes");
     //   String resultadointeres= requests.getParameter("resultadointeres");
                
        int capitalInt=Integer.parseInt(capital);         
        int aniosInt=Integer.parseInt(anios);    
        int interesInt=Integer.parseInt(interes);          
     // int resultadointeresInt=Integer.parseInt(resultadointeres);          
                        
        Calculadora cal = new Calculadora();
       
        cal.setCapital(capitalInt);
        cal.setAños(aniosInt);
        cal.setInteres(interesInt);
        cal.resultadointeres();
        cal.getResultadoInteres();
   
System.out.println("capitalInt"+capitalInt);
System.out.println("aniosInt"+aniosInt);
System.out.println("interesInt"+interesInt);
System.out.println("cal.getResultadoInteres()"+cal.getResultadoInteres());

       request.setAttribute("Calculadora", cal);
       request.getRequestDispatcher("resultado.jsp").forward(request, response);
                    
        
/**System.out.println("Pasando por POST1");    
        String capital= requests.getParameter("capital");
        String anios= requests.getParameter("anios");
        String interes= requests.getParameter("interes");   
request.getRequestDispatcher("index.jsp").forward(request, response);
*/   

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */

    /**@Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
*/
    
    
    
}
